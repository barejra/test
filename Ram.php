<?php

use \myNs2\User;

trait PrintInfoTrait {
    public function printProduct() {
        return "Ram: " . $this->name
            . "; price: " . $this->price
            . "; owner: " . $this->owner->getName();
    }
}

class Ram extends Product
{
    use PrintInfoTrait;

    private string $name;
    private float $price;
    private User $owner;
    private string $type;
    private int $memory;

    public function __construct(
        string $name,
        float $price,
        User $owner,
        string $type,
        int $memory
    )
    {
        $this->name = $name;
        $this->price = $price;
        $this->owner = $owner;
        $this->type = $type;
        $this->memory = $memory;

        parent::registerProduct($this);

    }

    public function __toString(): string
    {
        return printProduct(); //??Undefined function 'printProduct'
    }
}